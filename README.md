# Hello World

A simple dockerized hello world in go with echo.

## Guidelines

This project respects the [google guidelines](https://github.com/golang-standards/project-layout).
