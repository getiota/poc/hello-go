BINDIR     := $(CURDIR)/bin

BINNAME	   ?= hello

# Variables
PORT ?= 8000

# go options
GOFLAGS    ?=
EXT_LDFLAGS ?=
LDFLAGS    = -w -s -X main.exposedPort=$(PORT) $(EXT_LDFLAGS)
SRC        := $(shell find . -type f -name '*.go' -print)

# .env

ifneq (,$(wildcard ./.env))
    include .env
    export
endif

# rules

.PHONY: build
build: $(BINDIR)/$(BINNAME)

$(BINDIR)/$(BINNAME): $(SRC)
	go build $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' -o $(BINDIR)/$(BINNAME) ./cmd/$(BINNAME)

.PHONY: run
run:
	go run $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' ./cmd/$(BINNAME)

.PHONY: test
test:
	go test ./... -count=1

.PHONY: clean
clean:
	@rm -rf $(BINDIR)