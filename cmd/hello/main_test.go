package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

const payload = "eyJpc3MiOiJnZXRpb3RhLmV1Iiwic3ViIjoiY2xpZW50MSIsInJvbGVzIjpbImNsaWVudCJdLCJpYXQiOjE1OTc5MTc2MDYsImV4cCI6MTU5ODAwNDAwNn0"

func TestExtractJWTPayload(t *testing.T) {
	payload, err := decodeJWTPayload(payload)

	assert.Nil(t, err)
	assert.NotNil(t, payload)
}

func TestExtractJWTSubject(t *testing.T) {
	client, user, err := extractJWTSubject(payload)

	assert.Nil(t, err)
	assert.Equal(t, "client1", client)
	assert.Equal(t, "", user)
}
