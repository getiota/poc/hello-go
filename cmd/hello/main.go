package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"os"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var exposedPort string

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/hello", hello)

	e.GET("/world", world)

	e.POST("/transfer", transfer)

	// Start server
	e.Logger.Fatal(e.Start(":" + exposedPort))
}

// Handler
func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World2!\n"+os.Getenv("SECRET"))
}

func world(c echo.Context) error {
	return c.String(http.StatusOK, "World, Hello!\n"+os.Getenv("SECRET"))
}

func transfer(c echo.Context) error {
	var sb strings.Builder
	sb.WriteString("Transfer Response")

	client, user, _ := extractJWTSubject(c.Request().Header.Get("JWT"))
	sb.WriteString("\nClient: ")
	sb.WriteString(client)
	sb.WriteString("\nUser: ")
	sb.WriteString(user)

	requestDump, err := httputil.DumpRequest(c.Request(), true)
	if err == nil {
		sb.WriteString("\n\nRequest:\n")
		sb.WriteString(string(requestDump))
		sb.WriteString("\n")
	} else {
		fmt.Println(err)
	}

	transferTo := c.FormValue("transfer")

	sb.WriteString("\nTransfer to: ")
	sb.WriteString(transferTo)

	if transferTo != "" {
		resp, err := http.Get("http://" + transferTo)
		if err == nil {
			body, _ := ioutil.ReadAll(resp.Body)
			resp.Body.Close()

			sb.WriteString("\nStatus Code: ")
			sb.WriteString(fmt.Sprint(resp.StatusCode))
			if len(body) > 0 {
				sb.WriteString("\nBody: ")
				sb.WriteString(string(body))
			}
		} else {
			sb.WriteString("\nError: ")
			sb.WriteString(fmt.Sprint(err))
		}
	}

	return c.String(http.StatusOK, sb.String())
}

func decodeJWTPayload(payload string) (map[string]json.RawMessage, error) {
	raw, err := base64.RawURLEncoding.DecodeString(payload)
	if err != nil {
		return nil, err
	}

	var data map[string]json.RawMessage
	err = json.Unmarshal(raw, &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func extractJWTSubject(payload string) (client string, user string, err error) {
	data, err := decodeJWTPayload(payload)
	if err != nil {
		return
	}

	var subject string
	err = json.Unmarshal(data["sub"], &subject)
	if err != nil {
		return
	}

	parts := strings.SplitN(subject, "/", 2)
	client = parts[0]
	if len(parts) > 1 {
		user = parts[1]
	}
	return
}
